// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout() виконує функцію (код) через заданий відрізок часу. setInterval() - циклічно виконує функцію/код через вказаний інтервал.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// setTimeout() з нульовою затримкоюу просто змушує функцію викликатися наприкінці черги подій.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Для завершення циклу.

const ImgCollection = document.querySelectorAll("img");
const btnStart = document.querySelector(".start");
const btnStop = document.querySelector(".stop");
btnStart.disabled = true;

let i = 1;
function changeImg() {
  ImgCollection.forEach((elem) => (elem.className = "image-clear"));

  i++;
  document.getElementById(`${i}`).className = "image-to-show";
  if (i == ImgCollection.length) {
    i = 0;
  }
}
let intervalID = setInterval("changeImg()", 3000);
btnStop.addEventListener("click", () => {
  clearInterval(intervalID);
  btnStop.disabled = true;
  btnStart.disabled = false;
});
btnStart.addEventListener("click", () => {
  intervalID = setInterval("changeImg()", 3000);
  btnStop.disabled = false;
  btnStart.disabled = true;
});
